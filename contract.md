﻿Independent Contractor Agreement
================================

This Independent Contractor Agreement (“Agreement”) is made and entered into by the undersigned parties: __NAME__ (known as the “Company”) and __NAME__ (known as the “Contractor”).

In consideration of the promises, rights and obligations set forth below, the parties hereby agree as follows:

1. __Term__

    The term of this Agreement shall begin on __DATE__ and continue until __DATE__, unless terminated earlier as set forth in this Agreement. The term of this Agreement may be extended by mutual agreement between the parties.

1. __Services__

    The Contractor will provide the following services:

    ___Insert description of Services___

    The Contractor shall take direction from [Report] or as directed by Company’s Board of Directors. Additional services or amendments to the services described above may be agreed upon between the parties.

1. __Compensation__

    Subject to providing the services as outlined above, the Contractor will be paid the sum of $XX per day/week/month/upon completion of the services, plus GST. The Company will be invoiced weekly/biweekly/monthly/upon completion of the services, with payment due within XX business days of receipt of the invoice.

1. __Relationship__

    The Contractor will provide the Contractor’s services to the Company as an independent contractor and not as an employee.

    Accordingly:

    * The Contractor agrees that the Company shall have no liability or responsibility for the withholding, collection or payment of any taxes, employment insurance premiums or Canada Pension Plan contributions on any amounts paid by the Company to the Contractor or amounts paid by the Contractor to its employees or contractors. The Contractor also agrees to indemnify the Company from any and all claims in respect to the Company’s failure to withhold and/or remit any taxes, employment insurance premiums or Canada Pension Plan contributions.

    * The Contractor agrees that as an independent contractor, the Contractor will not be qualified to participate in or to receive any employee benefits that the Company may extend to its employees. 

    * The Contractor is free to provide services to other clients, so long as such other clients are not in competition with the Company and so long as there is no interference with the Contractor’s contractual obligations to the Company.

    * __The Contractor has no authority to and will not exercise or hold itself out as having any authority to enter into or conclude any contract or to undertake any commitment or obligation for, in the name of or on behalf of the Company.__

1. __Confidentiality and Intellectual Property__

    The Contractor hereby acknowledges that it has read and agrees to be bound by the terms and conditions of the Company’s confidentiality and proprietary information agreement attached hereto as Schedule “A” and which forms an integral part of this Agreement. If the Contractor retains any employees or contractors of its own who will perform services hereunder, the Contractor shall ensure that such employees or contractors execute an agreement no less protective of the Company’s intellectual property and confidential information than the attached agreement.

    The Contractor hereby represents and warrants to the Company that it is not party to any written or oral agreement with any third party that would restrict its ability to enter into this Agreement or the Confidentiality and Proprietary Information Agreement or to perform the Contractor’s obligations hereunder and that the Contractor will not, by providing services to the Company, breach any non-disclosure, proprietary rights, non-competition, non-solicitation or other covenant in favor of any third party.

    The Contractor hereby agrees that, during the term of this Agreement and for one (1) year following the termination hereof, the Contractor will not (i) recruit, attempt to recruit or directly or indirectly participate in the recruitment of any Company employee or (ii) directly or indirectly solicit, attempt to solicit, canvass or interfere with any customer or supplier of the Company in a manner that conflicts with or interferes in the business of the Company as conducted with such customer or supplier.

1. __Termination__

    The independent contractor relationship contemplated by this Agreement is to conclude on DATE unless terminated earlier as set forth below. The Contractor agrees that no additional advance notice or fees in lieu of notice are required in the event the relationship terminates on DATE.

    The Contractor agrees that the Company may terminate this Agreement at any time without notice or any further payment if the Contractor is in breach of any of the terms of this Agreement.

    The Company may terminate this Agreement at any time at its sole discretion, upon providing to the Contractor XX calendar days advance written notice of its intention to do so or payment of fees in lieu thereof.

    The Contractor may terminate this Agreement at any time at its sole discretion upon providing to the Company XX calendar days notice of Contractor’s intention to do so. Upon receipt of such notice the Company may waive notice in which event this Agreement shall terminate immediately.

1. __Obligations Surviving Termination of this Agreement__

    All obligations to preserve the Company’s Confidential Information, Intellectual Property and other warranties and representations set forth herein shall survive the termination of this Agreement.

1. __Entire Agreement__

    This Agreement, together with the Confidentiality and Proprietary Information Agreement, represents the entire agreement between the parties and the provisions of this Agreement shall supersede all prior oral and written commitments, contracts and understandings with respect to the subject matter of this Agreement. This Agreement may be amended only by mutual written agreement of the party.

1. __Assignment__

    This Agreement shall inure to the benefit of and shall be binding upon each party’s successors and assigns. Neither party shall assign any right or obligation hereunder in whole or in part, without the prior written consent of the other party.

1. __Governing Law and Principles of Construction__

    This Agreement shall be governed and construed in accordance with Ontario law. If any provision in this Agreement is declared illegal or unenforceable, the provision will become void, leaving the remainder of this Agreement in full force and effect.  

__IN WITNESS WHEREOF__, the parties hereto have caused this Agreement to be executed by their duly authorized representatives, effective as of the day and year first above written.

COMPANY REPRESENTATIVE            CONTRACTOR

By: _________________________     By: __________________________

Name: _______________________     Name: ________________________

Date: _______________________     Date: _________________________

<div style="page-break-after: always;"></div>

# Schedule “A”

## CONFIDENTIALITY AND PROPRIETARY INFORMATION AGREEMENT

In consideration of your engagement as an independent contractor or consultant with __[company]__ (the __“Company”__), the undersigned (the __“Consultant”__) agrees and covenants as follows:

1. Engagement with the Company as an independent contractor or consultant (__“Engagement”__) will give the Consultant access to proprietary and confidential information belonging to the Company, its customers, its suppliers and others (the proprietary and confidential information is collectively referred to in this Agreement as __“Confidential Information”__). Confidential Information includes but is not limited to customer lists, marketing plans, proposals, contracts, technical and/or financial information, databases, software and know-how. All Confidential Information remains the confidential and proprietary information of the Company.

1. As referred to herein, the “Business of the Company” shall relate to the business of the Company as the same is determined by the Board of Directors of the Company from time to time.

1. The Consultant may in the course of the Consultant’s Engagement with the Company conceive, develop or contribute to material or information related to the Business of the Company, including, without limitation, software, technical documentation, ideas, inventions (whether or not patentable), hardware, know-how, marketing plans, designs, techniques, documentation and records, regardless of the form or media, if any, on which such is stored (referred to in this Agreement as __“Proprietary Property”__). The Company shall exclusively own, and the Consultant does hereby assign to the Company, all Proprietary Property which the Consultant conceives, develops or contributes to in the course of the Consultant’s Engagement with the Company and all intellectual and industrial property and other rights of any kind in or relating to the Proprietary Property, including but not limited to all copyright, patent, trade secret and trade-mark rights in or relating to the Proprietary Property. Material or information conceived, developed or contributed to by the Consultant outside work hours on the Company’s premises or through the use of the Company’s property and/or assets shall also be Proprietary Property and be governed by this Agreement if such material or information relates to the Business of the Company. The Consultant shall keep full and accurate records accessible at all times to the Company relating to all Proprietary Property and shall promptly disclose and deliver to the Company all Proprietary Property.

1. The Consultant shall, both during and after the Consultant’s Engagement with the Company, keep all Confidential Information and Proprietary Property confidential and shall not use any of it except for the purpose of carrying out authorized activities on behalf of the Company. The Consultant may, however, use or disclose Confidential Information which:

    (i) is or becomes public other than through a breach of this Agreement;
    (ii) is known to the Consultant prior to the date of this Agreement and with respect to which the Consultant does not have any obligation of confidentiality; or
    (iii) is required to be disclosed by law, whether under an order of a court or government tribunal or other legal process, provided that Consultant informs the Company of such requirement in sufficient time to allow the Company to avoid such disclosure by the Consultant.

    The Consultant shall return or destroy, as directed by the Company, Confidential Information, Proprietary Property and any other Company property to the Company upon request by the Company at any time. The Consultant shall certify, by way of affidavit or statutory declaration, that all such Confidential Information, Proprietary Property or Company property has been returned or destroyed, as applicable.

1. The Consultant covenants and agrees not to make any unauthorized use whatsoever of or to bring onto the Company’s premises for the purpose of making any unauthorized use whatsoever of any trade secrets, confidential information or proprietary property of any third party, including without limitation any trade-marks or copyrighted materials, during the course of the Consultant’s Engagement with the Company.

1. At the reasonable request and at the sole expense of the Company, the Consultant shall do all reasonable acts necessary and sign all reasonable documentation necessary in order to ensure the Company’s ownership of the Proprietary Property, the Company property and all intellectual and industrial property rights and other rights in the same, including but not limited to providing to the Company written assignments of all rights to the Company and any other documents required to enable the Company to document rights to and/or register patents, copyrights, trade-marks, industrial designs and such other protections as the Company considers advisable anywhere in the world.

1. The Consultant hereby irrevocably and unconditionally waives all moral rights the Consultant may now or in the future have in any Proprietary Property.

1. The Consultant agrees that the Consultant will, if requested from time to time by the Company, execute such further reasonable agreements as to confidentiality and proprietary rights as the Company’s customers or suppliers reasonably require to protect confidential information or proprietary property.

1. Regardless of any changes in position, fees or otherwise, including, without limitation, termination of the Consultant’s Engagement with the Company, unless otherwise stipulated pursuant to the terms hereof, the Consultant will continue to be subject to each of the terms and conditions of this Agreement and any other(s) executed pursuant to the preceding paragraph.

1. The Consultant agrees that the Consultant’s sole and exclusive remedy for any breach by the Company of this Agreement will be limited to monetary damages and in case of any breach by the Company of this Agreement or any other Agreement between the Consultant and the Company, the Consultant will not make any claim in respect of any rights to or interest in any Confidential Information or Proprietary Property.

1. The Consultant acknowledges that the services provided by the Consultant to the Company under this Agreement are unique. The Consultant further agrees that irreparable harm will be suffered by the Company in the event of the Consultant’s breach or threatened breach of any of his or her obligations under this Agreement, and that the Company will be entitled to seek, in addition to any other rights and remedies that it may have at law or equity, a temporary or permanent injunction restraining the Consultant from engaging in or continuing any such breach hereof. Any claims asserted by the Consultant against the Company shall not constitute a defence in any injunction action, application or motion brought against the Consultant by the Company.

1. This Agreement is governed by the laws of the Province of Ontario and the parties agree to the non-exclusive jurisdiction of the courts of the Province of Ontario in relation to this Agreement.

1. If any provision of this Agreement is held by a court of competent jurisdiction to be invalid or unenforceable, that provision shall be deleted and the other provisions shall remain in effect.

__IN WITNESS WHEREOF__ the Company and the Consultant have caused this Agreement to be executed as of the ___ day of _______________, 20__.

 __[COMPANY]__
 
 Per:  
 Name:  
 Title:

|__CONSULTANT__ | __WITNESS__ |
| ------------- | ----------- |
|Name: | Name: |
